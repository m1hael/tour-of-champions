///
// Project Title
//
// The project description goes here. You can use HTML to format your documentation.
//
// The stream project at https://bitbucket.org/m1hael/stream is an example on
// how such a document can look like in code and you can see the result at 
// http://iledocs.rpgnextgen.com , select project "Stream".
//
// @project project label
// @author author
// @date 2021-01-01
// @link https://my-project.com
///